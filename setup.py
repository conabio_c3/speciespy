# -*- coding: utf-8 -*-
# @Author: Raul Sierra
# @Date:   2018-11-08 10:02:48
# @Last Modified by:   Raul Sierra
# @Last Modified time: 2018-11-08 14:33:14
import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="SpeciesPy",
    version="0.0.1",
    author="Raul Sierra-Alcocer",
    author_email="raul.sierra@conabio.gob.mx",
    description="Python API to SPECIES",
    long_description="Python API to SPECIES, a tool to explore and analyze biodiversity databases",
    long_description_content_type="text/markdown",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
