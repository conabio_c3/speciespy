# -*- coding: utf-8 -*-
# @Author: Raul Sierra
# @Date:   2019-01-17 19:25:08
# @Last Modified by:   Raul Sierra
# @Last Modified time: 2019-01-18 18:18:40
import requests
import pandas as pd
import geopandas as gpd

API_URL = 'http://species.conabio.gob.mx/api-db-dev/niche/'


def get_taxon_id(name, level):
    raise NotImplementedError


def get_taxon_children_ids(taxon_name, taxon_level, children_level):
    query = {
        'field': children_level,
        'parentfield': taxon_level,
        'parentitem': taxon_name
    }

    endpoint = f'{API_URL}/getVariables'

    res = requests.post(endpoint, json=query)
    data = res['data']
    child_names = [d['name'] for d in data]

    raise NotImplementedError

def get_taxon_cells(name, level, grid_res=16):

    sp_info = get_species_summary(name)
    registers = get_species_registers(sp_info.spid)

    grid = grids.get_grid(region=1, resolution=grid_res)

    gpd.
    raise NotImplementedError

