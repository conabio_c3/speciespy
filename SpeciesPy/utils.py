# -*- coding: utf-8 -*-
# @Author: Raul Sierra
# @Date:   2018-11-13 19:38:29
# @Last Modified by:   Raul Sierra
# @Last Modified time: 2019-02-07 10:17:23

import geopandas as gpd
import json
from shapely.geometry import shape, Point


def get_point_coords(row, geom, coord_type):
    """Calculates coordinates ('x' or 'y') of a Point geometry"""

    if coord_type == 'x':
        return row[geom].x
    elif coord_type == 'y':
        return row[geom].y


def get_gdf_from_json_str(src_df, geojson_col='json_geom'):
    src_df['geometry'] = src_df[geojson_col].apply(json.loads)
    src_df['geometry'] = src_df['geometry'].apply(shape)

    src_gpd = gpd.GeoDataFrame(src_df, geometry='geometry')

    return src_gpd


def create_gpd_df_from_coords(dataframe, longitude_col='x', latitude_col='y'):

    dataframe['Coordinates'] = list(zip(dataframe[longitude_col],
                                        dataframe[latitude_col]))
    dataframe['Coordinates'] = dataframe['Coordinates'].apply(Point)
    dataframe = gpd.GeoDataFrame(dataframe, geometry='Coordinates')

    return dataframe
