# -*- coding: utf-8 -*-
# @Author: Raul Sierra
# @Date:   2019-02-18 13:33:53
# @Last Modified by:   Raul Sierra
# @Last Modified time: 2019-02-19 15:17:34

import pandas as pd


def load_taxonomy(source='snib', url='', simplify=True, ambiente='terrestre'):
    df = pd.read_csv(url, dtype=str)

    if simplify:
        selected_cols = set([
                            'IdCAT',
                            'UltimaCategoriaTaxonomica',
                            'clase',
                            'Reino',
                            'orden',
                            'familia',
                            'genero',
                            'especie_epiteto',
                            'iucn',
                            'nom059',
                            'nivelprioridad',
                            'invasoras',
                            'endemismo',
                            'ambiente',
                            'nombrecomun'])

        diff_cols = selected_cols.difference(df.columns.values)
        assert diff_cols == set(), diff_cols

        df = df.loc[df.UltimaCategoriaTaxonomica.isin(['especie']) &
                    (df.EstatusTaxon == 'válido') &
                    (df.ambiente.str.contains('Terrestre')),
                    list(selected_cols)]

        return df


def load_occurrences(source='snib',
                     url='',
                     species_list=list(),
                     simplify=True):

    df = pd.read_csv(url, dtype=str, usecols=['especie', 'longitud', 'latitud', 'ambiente', 'idnombrecatvalido'])
    df = df.loc[df.especie.isin(species_list)]

    return df
