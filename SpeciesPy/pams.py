# -*- coding: utf-8 -*-
# @Author: Raul Sierra
# @Date:   2019-01-21 16:29:33
# @Last Modified by:   Raul Sierra
# @Last Modified time: 2019-07-22 13:29:18
import geopandas as gpd
import pandas as pd
from scipy import sparse
import numpy as np
import multiprocessing as mp
from itertools import repeat

from SpeciesPy import niche
import logging


def from_geojson_grid(geojson_file=''):
    """Summary

    Args:
        fname (str, optional): Description
    """

    data_gdf = gpd.read_file(geojson_file)
    grid_ids = data_gdf.gridid.unique()
    species_lst = [
        col for col in data_gdf.columns.values if len(col.split()) == 2
    ]

    species_lst.sort()
    grid_ids.sort()

    cells_idx = pd.Index(grid_ids)
    species_idx = pd.Index(species_lst)

    PAM = sparse.dok_matrix((len(cells_idx), len(species_idx)), dtype=np.int16)

    for i, row in data_gdf.iterrows():
        pam_i = cells_idx.get_loc(row['gridid'])

        for p in list(row[row == True].index):
            if species_idx.contains(p):
                pam_j = species_idx.get_loc(p)
                PAM[pam_i, pam_j] = 1

    PAM_snib = PAM.tocsr()

    return PAM_snib, species_lst, grid_ids


def from_grid_table(geojson_file):
    """Summary

    Args:
        geojson_file (str): path of geojson file
    """

    data_gdf = gpd.read_file(geojson_file)
    grid_ids = data_gdf.gridid.unique()

    presences_gdf = data_gdf[data_gdf.especie.notnull()]
    species_lst = presences_gdf.especie.unique()

    species_lst.sort()
    grid_ids.sort()

    cells_idx = pd.Index(grid_ids)
    species_idx = pd.Index(species_lst)

    PAM = sparse.dok_matrix((len(cells_idx), len(species_idx)), dtype=np.int16)

    for i, row in presences_gdf.iterrows():
        pam_i = cells_idx.get_loc(row['gridid'])
        pam_j = species_idx.get_loc(row['especie'])
        PAM[pam_i, pam_j] = 1

    PAM_snib = PAM.tocsr()

    return PAM_snib, species_lst, grid_ids


def to_csv(pam, file_name):
    dense = pam.toarray()
    dense.to_csv(file_name)


def from_occurrence_table(table, species_col, cells_col, cols, rows):
    table = table.copy()
    table['presence'] = 1
    data_pam = table.pivot_table(index=cells_col,
                                 columns=species_col,
                                 values='presence',
                                 fill_value=0)

    data_pam = data_pam.reindex(sorted(cols), axis=1, fill_value=0)
    data_pam = data_pam.reindex(sorted(rows), axis=0, fill_value=0)

    return data_pam


def table_from_species_score_grid(species_name,
                                  score_grid,
                                  base_grid,
                                  presence_threshold=0):

    score_grid_df = pd.DataFrame(score_grid['data_score_cell'])
    score_grid_df['species_name'] = species_name

    if 'gridid' in score_grid_df.columns:
        table = pd.merge(base_grid, score_grid_df, on='gridid')
        table['presence'] = 0
        table.loc[table.tscore > presence_threshold, 'presence'] = 1
    else:
        print(f'No column gridid in {score_grid_df} for {species_name}')
        table = base_grid.copy()
        table['presence'] = 0
        table['tscore'] = None

    return table


def get_SPECIES_presence_table(species_list,
                               base_grid,
                               analysis_res=64,
                               presence_threshold=0,
                               use_mp=True,
                               num_cores=1):

    analysis_res = analysis_res
    region = 1
    include_no_date = True
    include_fossil = True
    min_cells = 5
    use_apriori = True
    abiotic_covars = ['worldclim']
    count = 0

    if use_mp:
        repeated_args = [
            analysis_res, region, include_no_date, include_fossil, min_cells,
            use_apriori, abiotic_covars
        ]

        sdm_grid_args = zip(species_list, repeat(repeated_args))
        pool_args = [list([args[0]]) + args[1] for args in sdm_grid_args]

        logging.debug(f'from_species_list::request_model: {pool_args})')

        p = mp.Pool(num_cores)
        sp_grids = p.starmap(niche.get_species_sdm_grid, pool_args)
    else:
        sp_grids = []
        for sp_name in species_list:
            if count % 10 == 0:
                logging.info(
                    f'pams.from_species_list: requesting model number {count} \
                    ({sp_name})')

            sp_grids.append(
                niche.get_species_sdm_grid(sp_name, analysis_res, region,
                                           include_no_date, include_fossil,
                                           min_cells, use_apriori,
                                           abiotic_covars))
            count += 1

    logging.info(f'pams.from_species_list: Appending {count} SPECIES models)')

    sp_tables_args = zip(species_list, sp_grids)
    sp_tables_list = []
    count = 0

    for sp, sp_grid in sp_tables_args:
        if count % 10 == 0:
            logging.info(
                f'pams.from_species_list: Appending row {count} for {sp}')

        sp_tables_list.append(
            table_from_species_score_grid(sp, sp_grid, base_grid))

        count += 1
    sp_table = pd.concat(sp_tables_list, sort=False)

    return sp_table
