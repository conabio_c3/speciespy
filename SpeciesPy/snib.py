# -*- coding: utf-8 -*-
# @Author: Raul Sierra
# @Date:   2018-11-21 12:46:26
# @Last Modified by:   Raul Sierra
# @Last Modified time: 2019-02-18 13:33:42

import requests as req
import pandas as pd

API_URL = 'http://species.conabio.gob.mx/api-db-dev/niche/'

FIELDS_MAP = {
    'species': 'especievalidabusqueda',
    'genus': 'generovalido',
    'family': 'familiavalida',
    'order': 'ordenvalido',
    'class': 'clasevalida',
    'phylum': 'phylumdivisionvalido',
    'kingdom': 'reinovalido',
}


def get_taxon_species(taxon_rank, taxon_name):
    """Summary

    Args:
        taxon_rank (TYPE): Description
        taxon_name (TYPE): Description

    Returns:
        TYPE: Description
    """
    endpoint = API_URL + 'especie/getVariables'

    params = {
        'field': FIELDS_MAP['species'],
        'footprint_region': 1,
        'parentfield': FIELDS_MAP[taxon_rank],
        'parentitem': taxon_name,
    }

    res = req.post(endpoint, json=params)
    data = pd.DataFrame(res.json()['data'])

    return data


def get_species_summary(name,
                        analysis_res=16,
                        region=1,
                        include_no_date=True,
                        include_fossil=True):
    """ Get the taxonomical information, the number of registers in the DB,
    and the number of cells the species occupies for the specified grid.

    Keyword arguments:
        name -- species name
        analysis_res -- resolution of the target grid for the analysis
        region -- region of interest. 1 is Mexico, 2 is USA,
                3 is Mexico and the USA, 4 is Colombia
        include_fossil -- count fossils
        include_no_date -- count registers whithout a date specified
        api_url -- the API url
      """

    api_url = API_URL
    query = {
        'footprint_region': 1,
        'grid_res': analysis_res,
        'searchStr': name,  # substring de la especie a buscar
        'source': 1,
        'limit': 'false',
    }

    r = req.post(api_url + '/especie/getEntList', params=query)

    response = r.json()
    df = pd.DataFrame(response['data'])

    if 'especievalidabusqueda' in df.columns:
        return df.loc[df['especievalidabusqueda'] == name]
    else:
        return pd.DataFrame()

# def get_taxon_parent(taxon_name, taxon_rank, parent_rank, parent_name):