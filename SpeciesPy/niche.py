# -*- coding: utf-8 -*-
"""Summary

Attributes:
    API_URL (TYPE): Description
    API_versions (list): Description
    COVARS_DICT (TYPE): Description
    TAXA_RANK_DICT (TYPE): Description
"""
# @Author: Raul Sierra
# @Date:   2018-11-08 10:23:19
# @Last Modified by:   Raul Sierra
# @Last Modified time: 2019-07-23 08:50:09
import requests as req
import json
from json import JSONDecodeError
import pandas as pd
import geopandas as gpd
from shapely.geometry import Point
from sklearn.cluster import MeanShift
import numpy as np
import logging

API_versions = ['api-rc', 'api-db-dev', 'api']

API_URL = f'http://species.conabio.gob.mx/{API_versions[1]}/niche/'

COVARS_DICT = {
    'biotic': 0,
    'worldclim': 1,
    'climond': 2,
    'snmb': 3,
    'ENVIREM': 4,
    'USGS': 5,
}

TAXA_RANK_DICT = {
    'species': 'especievalidabusqueda',
    'genus': 'generovalido',
    'family': 'familiavalida',
    'order': 'ordenvalido',
    'class': 'clasevalida'
}


def get_species_summary(name,
                        analysis_res=16,
                        region=1,
                        include_no_date="false",
                        include_fossil="false"):
    """Get the taxonomical information, the number of registers in the DB,
    and the number of cells the species occupies for the specified grid.

    Keyword arguments:
        name -- species name
        analysis_res -- resolution of the target grid for the analysis
        region -- region of interest. 1 is Mexico, 2 is USA,
                3 is Mexico and the USA, 4 is Colombia
        include_fossil -- count fossils
        include_no_date -- count registers whithout a date specified
        api_url -- the API url

    Args:
        name (TYPE): Description
        analysis_res (int, optional): Description
        region (int, optional): Description
        include_no_date (str, optional): Description
        include_fossil (str, optional): Description

    Returns:
        TYPE: Description
    """

    api_url = API_URL
    query = {
        'footprint_region': 1,
        'grid_res': analysis_res,
        'searchStr': name,  # substring de la especie a buscar
        'source': 1,
        'limit': 'false',
        'sfecha': include_no_date,
        'sfosil': include_fossil
    }

    r = req.post(api_url + '/especie/getEntList', json=query)

    response = r.json()
    df = pd.DataFrame(response['data'])

    return df.loc[df['especievalidabusqueda'] == name]


def get_species_registers(spid,
                          analysis_res=16,
                          region=1,
                          include_no_date="false",
                          include_fossil="false"):
    """Obtiene los registros de una especie por id

    Args:
        spid (TYPE): Description
        analysis_res (int, optional): Description
        region (int, optional): Description
        include_no_date (str, optional): Description
        include_fossil (str, optional): Description

    Returns:
        TYPE: Description
    """

    api_url = API_URL
    query = {
        'id': int(spid),
        'sfecha': include_no_date,
        'sfosil': include_fossil,
        'grid_res': str(analysis_res),
        'footprint_region': str(region)
    }

    r = req.post(api_url + '/especie/getSpecies', json=query)

    response = r.json()
    df = pd.DataFrame(response['data'])

    if df.empty:
        df = pd.DataFrame({
            'aniocolecta': [0],
            'gridid': [-1],
            'json_geom': [{}],
            'occ': [0],
            'urlejemplar': ['']
        })
    return df


def get_species_sdm_grid(species_name,
                         analysis_res=16,
                         region=1,
                         include_no_date=True,
                         include_fossil=True,
                         min_cells=5,
                         use_apriori=True,
                         abiotic_covars=[]):
    """Summary

    Args:
        species_name (TYPE): Description
        analysis_res (int, optional): Description
        region (int, optional): Description
        include_no_date (bool, optional): Description
        include_fossil (bool, optional): Description
        min_cells (int, optional): Description
        abiotic_covars (list, optional): Description

    Returns:
        TYPE: Description
    """
    api_url = API_URL
    sdm_scores_service_url = api_url + '/countsTaxonsGroup'

    query = {
        "target_taxons": [
            {
                "taxon_rank": "species",
                "value": species_name
            }
        ],
        "apriori": use_apriori,
        "mapa_prob": False,
        "min_cells": min_cells,
        "fosil": include_fossil,
        "date": include_no_date,
        "idtabla": "",
        "grid_resolution": analysis_res,
        "region": region,
        "excluded_cells": [],
        "target_name": "targetGroup",
        "iterations": 1,
        "covariables": [],
        "alpha": 0.5
    }

    for i, a_covar in enumerate(abiotic_covars):

        query['covariables'].append({
            "biotic": False,
            "name": a_covar,
            "merge_vars": [
                {
                    "rank": "type",
                    "value": 1,
                    "type": COVARS_DICT[a_covar],
                    "level": "bid"
                }
            ],
            "group_item": i + 1,
        })

    query.update({
        "with_data_freq": False,
        "with_data_score_cell": True,
        "with_data_freq_cell": False,
        "with_data_score_decil": False
    })

    logging.debug(f'get_species_sdm_grid:query: {query}')

    r = req.post(sdm_scores_service_url, json=query)

    try:
        response = r.json()
    except JSONDecodeError as e:
        print('JSONDecodeError: ')
        print(e)
        print(r.text)
    except Exception as e:
        print('Unexpected Error: ')
        print(e)

    if 'response' not in locals() or not response['ok']:
        response = {
            "ok": False,
            "data": [],
            "data_freq": [],
            "data_score_cell": [],
            "data_freq_cell": []
        }

    return response


def get_occurrences_scores(spid,
                           analysis_res=16,
                           region=1,
                           include_no_date="false",
                           include_fossil="false",
                           min_cells=5,
                           abiotic_covars=[]):
    """Summary
    Assigns a score to each occurrence record of a taxon. The score
    is based on the selected features for the grid cell that contains
    the record.


    Args:
        spid (int): The ID of the target species
        analysis_res (int, optional): Description
        region (int, optional): Description
        include_no_date (bool, optional): Description
        include_fossil (bool, optional): Description
        min_cells (int, optional): Description
        abiotic_covars (list, optional): Description

    Returns:
        GeoDataFrame: Description


    """

    sp_registers = get_species_registers(spid,
                                         analysis_res=analysis_res,
                                         region=region,
                                         include_no_date=include_no_date,
                                         include_fossil=include_fossil)

    score_data_json = get_species_sdm_grid(
        spid=spid,
        analysis_res=analysis_res,
        region=region,
        abiotic_covars=abiotic_covars,
        include_fossil=include_fossil,
        include_no_date=include_no_date)

    cell_scores = pd.DataFrame(score_data_json['data_score_cell'])

    if cell_scores.empty:
        registers_with_score = pd.DataFrame(
            columns=['gridid', 'tscore', 'json_geom'])
    else:
        cell_scores.tscore = pd.to_numeric(cell_scores.tscore)
        registers_with_score = pd.merge(
            cell_scores,
            sp_registers,
            how='right',
            on='gridid',
            validate='one_to_many')

    scores_df = gpd.GeoDataFrame(registers_with_score)
    scores_df['geometry'] = scores_df.json_geom.apply(
        lambda x: json.loads(x)['coordinates'])
    scores_df['geometry'] = scores_df.geometry.apply(Point)
    scores_df = scores_df.set_geometry('geometry')
    scores_df = scores_df[scores_df.tscore.notnull()].copy()

    return scores_df


def assign_score_registers(tar_registers,
                           species_name,
                           analysis_res=16,
                           region=1,
                           abiotic_covars=[],
                           include_fossil=False,
                           include_no_date=False):
    """Summary

    Args:
        tar_registers (TYPE): Description
        species_name (TYPE): Description
        analysis_res (int, optional): Description
        region (int, optional): Description
        abiotic_covars (list, optional): Description
        include_fossil (str, optional): Description
        include_no_date (str, optional): Description

    Returns:
        TYPE: Description
    """
    score_data_json = get_species_sdm_grid(
        species_name=species_name,
        analysis_res=analysis_res,
        region=region,
        abiotic_covars=abiotic_covars,
        include_fossil=include_fossil,
        include_no_date=include_no_date)

    cell_scores = pd.DataFrame(score_data_json['data_score_cell'])

    if cell_scores.empty:
        registers_with_score = pd.DataFrame(
            columns=['gridid', 'tscore', 'json_geom'])
    else:
        cell_scores.tscore = pd.to_numeric(cell_scores.tscore)
        registers_with_score = tar_registers.merge(
            cell_scores,
            on='gridid',
            validate='many_to_one')

    scores_df = registers_with_score
    try:
        scores_df = scores_df[scores_df.tscore.notnull()].copy()
    except TypeError as e:
        print(f'Scores error: {scores_df}')
        print(f'Error {e}')

    return scores_df


def mark_outliers(data, out_thres=0, bw=None):
    """Summary

    Args:
        data (TYPE): Description
        out_thres (int, optional): Description
        bw (None, optional): Description

    Returns:
        DataFrame: Description
    """

    if not data.empty:
        ms = MeanShift(bw)
        y = ms.fit_predict(data.tscore.values.reshape((-1, 1)))
        data['ms_cluster'] = y

        cluster_centers = ms.cluster_centers_
        cluster_idx_sort = cluster_centers[:, 0].argsort()

        data['ms_cluster'] = [np.where(cluster_idx_sort == c)[0][0]
                              for c in data['ms_cluster']]
        data['isoutlier'] = data[['ms_cluster', 'tscore']].groupby('ms_cluster').transform('max') < out_thres

    # if not data.empty:
    #     outlier_detector = MeanShift(bw)
    #     y = outlier_detector.fit_predict(data.tscore.values.reshape((-1, 1)))
    #     data['ms_cluster'] = y

    #     data['isoutlier'] = data[[
    #         'ms_cluster', 'tscore'
    #     ]].groupby('ms_cluster').transform('max') < out_thres

    return data


def get_token(sp_info,
              analysis_res=16,
              region=1,
              include_no_date="false",
              include_fossil="false",
              min_cells=5,
              abiotic_covars=[]):
    """Summary

    Args:
        sp_info (TYPE): Description
        analysis_res (int, optional): Description
        region (int, optional): Description
        include_no_date (str, optional): Description
        include_fossil (str, optional): Description
        min_cells (int, optional): Description
        abiotic_covars (list, optional): Description

    Returns:
        TYPE: Description
    """
    api_url = API_URL
    service_url = api_url + '/especie/getToken?'

    print(sp_info)

    sp_data_params = '{{"especie":"{0}","spid":{1},"label":"{0}"}}'.format(
        sp_info.especievalidabusqueda, sp_info.spid)
    filters_params = '&chkFosil={fossil}&chkFec={no_date}&chkOcc=5&gridRes={res}&region=1&num_dpoints=0&num_filters={num_cov}'.format(
        res=analysis_res,
        num_cov=len(abiotic_covars),
        fossil=str(include_fossil).lower(),
        no_date=str(include_no_date).lower())

    model_params = ''

    for i, covar in enumerate(abiotic_covars):
        model_params = model_params + '&tfilters[{0}]={{"value":[{{"value":"{1}","label":"Raster >> {1}","level":1,"type":{3}}}],"type":{3},"groupid":{2},"title":"Gpo Raster {2}"}}'.format(
            i, covar, i + 1, COVARS_DICT[covar])

    params = {
        'confparams':
        'sp_data={}{}{}'.format(sp_data_params, filters_params, model_params),
        'tipo':
        'nicho'
    }

    r = req.post(service_url, params)
    response = r.json()

    token = response['data'][0]['token']
    url = f'http://species.conabio.gob.mx/dbdev/geoportal_v0.1.html#link/?token={token}'

    return url


def get_species_infra_cats(sp_binomial, region=1):
    """Summary

    Args:
        sp_binomial (TYPE): Description
        region (int, optional): Description

    Returns:
        TYPE: Description
    """
    autocomplete_endpt = f'{API_URL}/especie/getEntList'
    subspecies = []

    if species_exists(sp_binomial, region=region):
        # Get list of taxa that contain the scientific name, this will include all infra species categories
        res = req.post(autocomplete_endpt, json={
            'limit': False,
            'searchStr': sp_binomial,
            'source': 0,
            'footprint_region': region,
        })
        data = res.json()

        subspecies = [d['especievalidabusqueda'] for d in data['data']]
    return subspecies


def get_taxa_children(taxa_name, taxa_rank, children_rank='species', region=1):
    """Summary

    Args:
        taxa_name (TYPE): Description
        taxa_rank (TYPE): Description
        children_rank (str, optional): Description
        region (int, optional): Description

    Returns:
        TYPE: Description
    """
    get_taxa_endpt = f'{API_URL}/especie/getVariables'

    # Get list of taxa that contain the scientific name, this will include all infra species categories
    res = req.post(get_taxa_endpt, json={
        'field': TAXA_RANK_DICT[children_rank],
        'parentfield': TAXA_RANK_DICT[taxa_rank],
        'parentitem': taxa_name,
        'footprint_region': region,
    })

    data = res.json()
    if data['data']:
        data['data'] = [d['name'] for d in data['data']]

    return data['data']


def species_exists(sp_binomial, region=1):
    """Summary

    Args:
        sp_binomial (TYPE): Description
        region (int, optional): Description

    Returns:
        TYPE: Description
    """
    get_taxa_endpt = f'{API_URL}/especie/getVariables'

    # Get list of taxa that contain the scientific name, this will include all infra species categories
    res = req.post(get_taxa_endpt, json={
        'field': TAXA_RANK_DICT['species'],
        'parentfield': TAXA_RANK_DICT['species'],
        'parentitem': sp_binomial,
        'footprint_region': region,
    })

    data = res.json()

    return bool(data['data'])


def species_bio_niche(sp_binomial, bio_covars, covars_rank):
    """Summary

    Args:
        sp_binomial (TYPE): Description
        bio_covars (TYPE): Description
        covars_rank (TYPE): Description

    Returns:
        TYPE: Description
    """
    modeling_endpt = f'{API_URL}/countsTaxonsGroup'

    subspecies_list = get_species_infra_cats(sp_binomial)

    model_spec = {
        "grid_resolution": 16,
        "region": 1,
        "min_cells": 5,
        "target_name": "Lynx_rufus",
        "target_taxons": [],
        "covariables_taxons": []
    }

    for sub_sp in subspecies_list:
        model_spec['target_taxons'].append(
            {
                "taxon_rank": "species",
                "value": sub_sp
            })

    for covar in bio_covars:
        model_spec['covariables_taxons'].append(
            {
                "group_name": covar['name'],
                "biotic": True,
                "group_taxons": [{"taxon_rank": covars_rank, "value": item} for item in covar['merge']]
            })

    res = req.post(modeling_endpt, json=model_spec)
    print(res)
    data = res.json()

    return data
