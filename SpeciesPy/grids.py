# -*- coding: utf-8 -*-
# @Author: Raul Sierra
# @Date:   2018-11-23 12:46:17
# @Last Modified by:   Raul Sierra
# @Last Modified time: 2019-02-07 17:15:50

import requests as req
import geopandas as gpd

API_URL = 'http://species.conabio.gob.mx/api-db-dev/niche/'


def get_grid(region=1, resolution=16):
    params = {
        'grid_res': resolution,
        'footprint_region': region,
    }

    response = req.post(API_URL + 'especie/getGridGeoJson', json=params)
    geojson = response.json()
    return gpd.GeoDataFrame.from_features(geojson['features'])


def project_points_to_grid(points, region=1, resolution=16):

    grid = get_grid(region, resolution)
    joined = gpd.sjoin(points, grid, how='inner', op='within')

    return joined


def sjoin_cells_with_points(points, grid):
    joined = gpd.sjoin(grid, points, how='left', op='contains')

    return joined
